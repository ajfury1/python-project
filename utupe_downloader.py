from pytube import YouTube
from tqdm import tqdm
import tkinter as tk
from tkinter import filedialog

root = tk.Tk()
root.withdraw() #use to hide tkinter window

class YoutupeDownloader:

    file_size = 0
    progress_bar = None  # Initialize progress bar as None

    def downloader(self):
        url = input('Enter Video Url: ')
        
        video = YouTube(url,
                       on_progress_callback=self.progress_func,
                       on_complete_callback=self.complete_func,
                       use_oauth=False,
                       allow_oauth_cache=True
                    )
        file_name = video.title

        # Download folder selection by user
        download_folder_path = filedialog.askdirectory()
        print('download folder:', download_folder_path)
        
        # video get and download processing
        print('Please wait, Download is Processing ...')
        stream = video.streams.filter(progressive=True, file_extension="mp4")
        selected_res = self.getPreferedResolution(stream)
        # filtered stream by prefered resolution
        stream = list(filter(lambda x: selected_res in x.resolution, stream))
        stream[0].download(download_folder_path, file_name)
        
    def getPreferedResolution(self, stream):
        for i in range(len(stream)):
            # get stream resolution
            # Thank to => https://stackoverflow.com/questions/65827515/fetch-youtube-video-resolution-in-python-pytube
            print(f'{i}. {stream[i].resolution}',end="  ")
        # get new line
        print()
        prefered_res = input('Select prefered resolution eg: 360 or 480 etc:')
        return prefered_res

    def progress_func(self, stream, data_chunk, bytes_remaining):
        self.file_size = stream.filesize
        bytes_downloaded = self.file_size - bytes_remaining
        self.show_progress(bytes_downloaded, False)

    def complete_func(self, stream, fileName):
        print('Successfully Downloaded...')
        self.show_progress(0, True)

    def show_progress(self, bytes_downloaded, isClose=False):
        if self.progress_bar is None:  # Initialize bar on first call
            self.progress_bar = tqdm(total=self.file_size, unit='B', unit_scale=True)
        
        if isClose:
            self.progress_bar.close()
        else:
            self.progress_bar.update(bytes_downloaded)  # Update progress bar


ytd = YoutupeDownloader()
ytd.downloader()



